<?php
class mysqldb {

    function __construct($dbusername, $dbpassword, $dbname, $host) {

        try {
            $pdo = new PDO("mysql:host=$host;dbname=$dbname", "$dbusername", "$dbpassword");
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Verbingung zur Datenbank fehlgeschlagen: " . $e->getMessage();
            return FALSE;
        }

        $this->connection=$pdo;

    }

}
?>