<?php
/**
 * DokuWiki Plugin ofbmgmnt (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Marcel Griesbach <ml@grsbch.de>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) {
    die();
}

//require the mysqldb class
require("mysqldb.php");

class syntax_plugin_ofbmgmnt extends DokuWiki_Syntax_Plugin
{

    private $dbhandle;

    /**
     * @return string Syntax mode type
     */
    public function getType()
    {
        return 'substition';
    }

    /**
     * @return string Paragraph type
     */
    public function getPType()
    {
        return 'block';
    }

    /**
     * @return int Sort order - Low numbers go before high numbers
     */
    public function getSort()
    {
        return 222;
    }

    /**
     * Connect lookup pattern to lexer.
     *
     * @param string $mode Parser mode
     */
    public function connectTo($mode)
    {  
        // Wenn das Pattern gematcht wird springt das Plugin zur Methode "handle" 
        // ("Handle Match"), dabei wird er gematchte String als $match übergeben und kann weiter 
        // verarbeitet werden - s.u. 
        //$this->Lexer->addSpecialPattern('\{\{projekt>.+?\}\}', $mode, 'plugin_ofbmgmnt');
        $this->Lexer->addSpecialPattern('\{\{ofb>.+?\}\}', $mode, 'plugin_ofbmgmnt');
    }

    /**
     * Handle matches of the projekt syntax
     *
     * @param string       $match   The match of the syntax
     * @param int          $state   The state of the handler
     * @param int          $pos     The position in the document
     * @param Doku_Handler $handler The handler
     *
     * @return array Data for the renderer
     */
    public function handle($match, $state, $pos, Doku_Handler $handler)
    {
        // Geschweifte Klammern entfernen
        $match = substr($match, 2, -2);
        // Splitten am ">". Erster Teil geht nach "command"
        list($identifier, $cmdopt) = explode('>', $match, 2);
        
        // Weitere Optionen können übergeben werden, wenn Bedarf ist
        //if (true/*str_contains($cmdopt, '#')*/) {
            list($cmd, $opt) = explode('#', $cmdopt, 2);
        /*} else {
            $cmd = $cmdopt;
            $opt = null;
        }*/

        // Das Array, das hier zurückgegeben wird, landet als $data 
        // in der render-Methode, s.u. 
        return array($cmd, $opt);
    }

    /**
     * Render xhtml output or metadata
     *
     * @param string        $mode     Renderer mode (supported modes: xhtml)
     * @param Doku_Renderer $renderer The renderer
     * @param array         $data     The data from the handler() function
     *
     * @return bool If rendering was successful.
     */
    public function render($mode, Doku_Renderer $renderer, $data)
    {
        // Wir rendern nur HTML, sonst nix!
        if ($mode !== 'xhtml') {
            return false;
        }

        // Die handle Methode liefert in $data ein Array mit 2 Elementen (s.o.)
        // das klamüsern wir jetzt mal auseinander:
        $cmd = $data[0];
        $opt = $data[1];

        // Breche die Verarbeitung ab, wenn der Benutzer nicht angemeldet ist.
        if ( ! isset($_SERVER['REMOTE_USER']) ) {
            $renderer->doc .= "<div class=\"warnmsg\">Um diesen Teil zu sehen, müssen Sie sich anmelden!</div>";
            return true;
        }

        //Set renderer as global var; for access from other methods;

        //Get a connection to the database
        $dbhost = $this->getConf("dbhost");
        $dbname = $this->getConf("dbname");
        $dbusername = $this->getConf("dbusername");
        $dbuserpassword = $this->getConf("dbuserpassword");

        $this->dbhandle = new mysqldb($dbusername, $dbuserpassword, $dbname, $dbhost);

        // Alles was mit dem Verkettungs-Operator "."
        // an $renderer->doc "angehängt" wird, 
        // wird als Ersetzung des gematche Patterns in der 
        // Wiki-Seite eingefügt. Der Browser will HTML sehen.
        //$renderer->doc .= "Ein Match kam zustande. Hier ist bald Inhalt!" . $cmd . ";" . $opt . "<br>";

        //$renderer->doc .= $this->printform();

        /*if (isset($_POST["inputName"])) {
            $id = $_POST["inputName"];
            $renderer->doc.= "<br>InputName: " . $_POST["inputName"] . "<br>";
        }*/

        /*$statement = $this->dbhandle->connection->prepare("SELECT * FROM Verleihobjekte;");
        $statement->execute(array());

        $res = array();
        while($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $res[] = $row;
        }

        print_r($res);*/
        

        /*foreach($res as $row) {
            $renderer->doc .= $row["id"] . " " . $row["Typ"] . " " . $row["Nummer"] . " " . $row["Bemerkung"] . "<br>";
        }*/

        switch($cmd) {
            case "rental":
                $renderer->doc .= $this->rental($opt);
                break;
            case "item":
                $renderer->doc .= $this->item($opt);
                break;
            case "credit":
                $renderer->doc .= $this->credit($opt);
                break;
            case "rooms":
                $renderer->doc .= $this->rooms($opt);
                break;
        }

        // Alles gut: 
        return true;
    }


    public function rental($opt) {
        $html = "";
        switch($opt) {
            case "list":
                //Frage Daten aus der DB ab.
                $statement = $this->dbhandle->connection->prepare("SELECT Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Objekttypen.id AS ObjID, Verleihobjekte.id AS voID, Verleihobjekte.Nummer, Verleihobjekte.Bemerkung, v1_verleihliste.lender, v1_verleihliste.lending, v1_verleihliste.pawn, v1_verleihliste.time FROM Objekttypen, Verleihobjekte, v1_verleihliste WHERE Objekttypen.id=Verleihobjekte.Typ AND Verleihobjekte.id=v1_verleihliste.objectID ORDER BY time ASC;");
                $statement->execute(array());
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                //Erstelle Tabelle mit den Daten
                $html .= "<table class=\"ofb-table\"> <tr class='ofb-tr'>  <th class='ofb-th'>Gegenstand</th> <th class='ofb-th'>Verleiher</th> <th class='ofb-th'>Leihender</th> <th class='ofb-th'>Pfand</th> <th class='ofb-th'>Uhrzeit</th>  </tr>";
                $i = 0;
                foreach($resArr as $res) {
                    if ($i % 2 == 0) {
                        $oddeven = "even";
                    } else {
                        $oddeven = "odd";
                    }
                    $i++;
                    //if ($this->getConf("ofb-name-short")) {
                        //$obj = $res["shortname"] . " ";
                    //} else {
                        $obj = $res["Name"] . " ";
                    //}
                    if ($res["shownr"] == 0) {
                        $obj .= $res["Bemerkung"];
                    } else {
                        $obj .= $res["Nummer"];
                    }
                    $html .= "<tr class='ofb-tr ofb-row-" . $oddeven . "'>  <td class='ofb-td'>" . $obj ."</td> <td class='ofb-td'>" . $res["lender"] . "</td> <td class='ofb-td'>" . $res["lending"] . "</td> <td class='ofb-td'>" . $res["pawn"] . "</td> <td class='ofb-td'>";
                    $unix = strtotime($res["time"]);
                    $html .= date("H:i", $unix) . " Uhr, " . date("d.m.Y", $unix);
                    $html .= "</td>  </tr>";
                }
                $html .= "</table><br>";
                break;


            case "rent":
                //Wenn Gegenstände ausgeliehen werden sollen, targe das in der DB ein
                if (isset($_POST["rental_rent_lender"]) && $_POST["rental_rent_lender"] !== "" && isset($_POST["rental_rent_lending"]) && $_POST["rental_rent_lending"] !== "" && isset($_POST["rental_rent_pawn"]) && $_POST["rental_rent_pawn"] !== "" && checkSecurityToken()) {
                    //Anzahl der zu verleihenden Gegenstände holen
                    $statement = $this->dbhandle->connection->prepare("SELECT id FROM Verleihobjekte;");
                    $statement->execute(array());
                    $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                    //Gegenstände auslesen
                    foreach($resArr as $id) {
                        $id = $id["id"];
                        if (isset($_POST["rental_rent_object_" . $id]) && $_POST["rental_rent_object_" . $id] !== "") {
                            if ($_POST["rental_rent_object_" . $id]) {
                                $statement =  $this->dbhandle->connection->prepare("INSERT INTO v1_verleihliste (id, objectID, lender, lending, pawn, time) VALUES (NULL, :id, :lender, :lending, :pawn, :time);");
                                $statement->execute(array(":id" => "$id", ":lender" => $_POST["rental_rent_lender"], ":lending" => $_POST["rental_rent_lending"], ":pawn" => $_POST["rental_rent_pawn"], ":time" => date("Y-m-d H:i:s")));
                            }
                        }
                    }
                }


                //Liste freie Gegenstände auf
                //Hole Liste verliehener Gegenstände
                $statement = $this->dbhandle->connection->prepare("SELECT Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Objekttypen.id AS ObjID, Verleihobjekte.id AS voID, Verleihobjekte.Nummer, Verleihobjekte.Bemerkung, v1_verleihliste.lender, v1_verleihliste.lending, v1_verleihliste.pawn, v1_verleihliste.time FROM Objekttypen, Verleihobjekte, v1_verleihliste WHERE Objekttypen.id=Verleihobjekte.Typ AND Verleihobjekte.id=v1_verleihliste.objectID ORDER BY time ASC;");
                $statement->execute(array());
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                //Hole Liste aller Gegenstände
                $statement2 = $this->dbhandle->connection->prepare("SELECT Verleihobjekte.id, Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Nummer, Bemerkung FROM Verleihobjekte, Objekttypen WHERE Verleihobjekte.Typ=Objekttypen.id ORDER BY Name, Nummer, Bemerkung;");
                $statement2->execute(array());
                $resArrItems = $statement2->fetchAll(PDO::FETCH_ASSOC);
                //Vereinheitliche die Liste ausgeliehenen Gegenstände
                $rentedItems = array();
                foreach($resArr as $rentItem) {
                    $curItem = serialize(array("id" => $rentItem["voID"], "name" => $rentItem["Name"], "shortname" => $rentItem["shortname"], "shownr" => $rentItem["shownr"], "nr" => $rentItem["Nummer"], "comment" => $rentItem["Bemerkung"]));
                    $newRentedItems = array_merge($rentedItems, array($curItem));
                    $rentedItems = $newRentedItems;
                }
                //Vereinheitliche die Liste aller Gegenstände
                $allItems = array();
                foreach($resArrItems as $allItem) {
                    $curItem = serialize(array("id" => $allItem["id"], "name" => $allItem["Name"], "shortname" => $allItem["shortname"], "shownr" => $allItem["shownr"], "nr" => $allItem["Nummer"], "comment" => $allItem["Bemerkung"]));
                    $newAllItems = array_merge($allItems, array($curItem));
                    $allItems = $newAllItems;
                }
                $freeItemsSer = array_diff($allItems, $rentedItems);
                $freeItems = array();
                foreach($freeItemsSer as $freeItemSer) {
                    array_push($freeItems, unserialize($freeItemSer));
                }
                $labels = array();
                foreach($freeItems as $item) {
                    if ($this->getConf("ofb-name-short") && $item["shortname"] != "") {
                        $obj = $item["shortname"] . " ";
                    } else {
                        $obj = $item["name"] . " ";
                    }
                    if ($item["shownr"] == 0) {
                        $obj .= $item["comment"];
                    } else {
                        $obj .= $item["nr"];
                    }
                    array_push($labels, $item["id"] . "#" . $obj);
                }

                //Erstelle Formular zum ausleiehen
                $form = new dokuwiki\Form\Form();
                foreach($labels as $labelInfos) {
                    list($id, $label) = explode('#', $labelInfos);
                    $form->addCheckbox("rental_rent_object_" . $id, $label);
                    $form->addHTML("&nbsp &nbsp &nbsp"); //FÜge 3 Leerzeichen zwischen die Checkboxen
                }
                $form->addHTML("<br><br>");
                $form->addTextInput("rental_rent_lender", "Verleiher:");
                $form->addHTML("<br>");
                $form->addTextInput("rental_rent_lending", "Leihender:");
                $form->addHTML("<br>");
                $form->addTextInput("rental_rent_pawn", "Pfand:");
                $form->addHTML("<br>");
                $form->addButton("submit", "ausleihen");
                $html .= $form->toHTML();
                break;


            case "return":
                //Wenn Gegenstände ausgeliehen werden sollen, targe das in der DB ein
                if (isset($_POST["rental_return"]) && checkSecurityToken()) {
                    //Anzahl der zu verleihenden Gegenstände holen
                    $statement = $this->dbhandle->connection->prepare("SELECT id FROM Verleihobjekte;");
                    $statement->execute(array());
                    $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                    //Gegenstände auslesen
                    foreach($resArr as $id) {
                        $id = $id["id"];
                        if (isset($_POST["rental_return_object_" . $id]) && $_POST["rental_return_object_" . $id] !== "") {
                            if ($_POST["rental_return_object_" . $id]) {
                                //Frage zu löschende Daten ab
                                $statement = $this->dbhandle->connection->prepare("SELECT * FROM v1_verleihliste WHERE objectID=?;");
                                $statement->execute(array($id));
                                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                                //Hole Zustand des Gegenstands, falls angegeben
                                $condition = "";
                                if (isset($_POST["rental_return_object_" . $id . "_condition"]) && $_POST["rental_return_object_" . $id . "_condition"] !== "") {
                                    $condition = $_POST["rental_return_object_" . $id . "_condition"];
                                }
                                //Speichere zu löschende Daten im Archiev
                                foreach($resArr as $res) {
                                    $statement = $this->dbhandle->connection->prepare("INSERT INTO v1_leiharchiv (id, objectID, lender, lending, pawn, time, returned, itemcondition) VALUES (NULL, :id, :lender, :lending, :pawn, :time, :returned, :condition);");
                                    $statement->execute(array(":id" => $res["objectID"], ":lender" => $res["lender"], ":lending" => $res["lending"], ":pawn" => $res["pawn"], ":time" => $res["time"], ":returned" => date("Y-m-d H:i:s"), ":condition" => $condition));
                                }
                                //Lösche Daten
                                $statement = $this->dbhandle->connection->prepare("DELETE FROM v1_verleihliste WHERE objectID=?;");
                                $statement->execute(array($id));
                            }
                        }
                    }
                }


                //Liste verliehene Gegenstände auf
                $statement = $this->dbhandle->connection->prepare("SELECT Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Objekttypen.id AS ObjID, Verleihobjekte.id AS voID, Verleihobjekte.Nummer, Verleihobjekte.Bemerkung, v1_verleihliste.lender, v1_verleihliste.lending, v1_verleihliste.pawn, v1_verleihliste.time FROM Objekttypen, Verleihobjekte, v1_verleihliste WHERE Objekttypen.id=Verleihobjekte.Typ AND Verleihobjekte.id=v1_verleihliste.objectID ORDER BY Name, Nummer, Bemerkung ASC;");
                $statement->execute(array());
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                //Vereinheitliche die Liste ausgeliehenen Gegenstände
                $rentedItems = array();
                foreach($resArr as $rentItem) {
                    $curItem = array("id" => $rentItem["voID"], "name" => $rentItem["Name"], "shortname" => $rentItem["shortname"], "shownr" => $rentItem["shownr"], "nr" => $rentItem["Nummer"], "comment" => $rentItem["Bemerkung"]);
                    $newRentedItems = array_merge($rentedItems, array($curItem));
                    $rentedItems = $newRentedItems;
                }
                $labels = array();
                foreach($rentedItems as $item) {
                    if ($this->getConf("ofb-name-short") && $item["shortname"] != "") {
                        $obj = $item["shortname"] . " ";
                    } else {
                        $obj = $item["name"] . " ";
                    }
                    if ($item["shownr"] == 0) {
                        $obj .= $item["comment"];
                    } else {
                        $obj .= $item["nr"];
                    }
                    array_push($labels, $item["id"] . "#" . $obj);
                }

                //Erstelle Formular zum zurückgeben
                $form = new dokuwiki\Form\Form();
                foreach($labels as $labelInfos) {
                    list($id, $label) = explode('#', $labelInfos);
                    $form->addCheckbox("rental_return_object_" . $id, $label);
                    $form->addHTML("&nbsp &nbsp &nbsp");
                    $form->addTextInput("rental_return_object_" . $id . "_condition", "Zustand:");
                    $form->addHTML("<br>");
                    $form->setHiddenField("rental_return", "true");
                }
                $form->addHTML("<br>");
                $form->addButton("submit", "zurückgeben");
                $html .= $form->toHTML();
                break;


            case "archive":
                //Frage Daten aus der DB ab.
                $statement = $this->dbhandle->connection->prepare("SELECT Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Objekttypen.id AS ObjID, Verleihobjekte.id AS voID, Verleihobjekte.Nummer, Verleihobjekte.Bemerkung, v1_leiharchiv.lender, v1_leiharchiv.lending, v1_leiharchiv.pawn, v1_leiharchiv.time, v1_leiharchiv.returned, v1_leiharchiv.itemcondition FROM Objekttypen, Verleihobjekte, v1_leiharchiv WHERE Objekttypen.id=Verleihobjekte.Typ AND Verleihobjekte.id=v1_leiharchiv.objectID AND returned>:lastDay ORDER BY returned ASC;");
                $statement->execute(array(":lastDay" => date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d")-1 , date("Y")))));
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                //Erstelle Tabelle mit den Daten
                $html .= "<table class=\"ofb-table\"> <tr class='ofb-tr'>  <th class='ofb-th'>Gegenstand</th> <th class='ofb-th'>Verleiher</th> <th class='ofb-th'>Leihender</th> <th class='ofb-th'>Pfand</th> <th class='ofb-th'>Uhrzeit</th> <th class='ofb-th'>Zustand des Gegenstands</th>  </tr>";
                $i = 0;
                foreach($resArr as $res) {
                    if ($i % 2 == 0) {
                        $oddeven = "even";
                    } else {
                        $oddeven = "odd";
                    }
                    $i++;
                    $obj = $res["Name"] . " ";
                    if ($res["shownr"] == 0) {
                        $obj .= $res["Bemerkung"];
                    } else {
                        $obj .= $res["Nummer"];
                    }
                    $html .= "<tr class='ofb-tr ofb-row-" . $oddeven . "'>  <td class='ofb-td'>" . $obj ."</td> <td class='ofb-td'>" . $res["lender"] . "</td> <td class='ofb-td'>" . $res["lending"] . "</td> <td class='ofb-td'>" . $res["pawn"] . "</td> <td class='ofb-td'>";
                    $unixRent = strtotime($res["time"]);
                    $unixReturn = strtotime($res["returned"]);
                    if (date("Y-m-d", $unixRent) == date("Y-m-d", $unixReturn)) {
                        $html .= date("H:i", $unixRent) . " - " . date("H:i", $unixReturn) . " Uhr, " . date("d.m.Y", $unixReturn);
                    } else {
                        $html .= date("H:i", $unixRent) . " Uhr, " . date("d.m.Y", $unixRent) . " - " . date("H:i", $unixReturn) . " Uhr, " . date("d.m.Y", $unixReturn);
                    }
                    $html .= "</td> <td class='ofb-td'>" . $res["itemcondition"] . "</td>  </tr>";
                }
                $html .= "</table><br>";
                break;

            case "deleted":
                //Das selbe Spiel nochmal für Kaputte Gegenstände
                $statement = $this->dbhandle->connection->prepare("SELECT Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Objekttypen.id AS ObjID, Verleihobjektearchiv.id AS voID, Verleihobjektearchiv.nr, Verleihobjektearchiv.comment, v1_leiharchiv.lender, v1_leiharchiv.lending, v1_leiharchiv.pawn, v1_leiharchiv.time, v1_leiharchiv.returned, v1_leiharchiv.itemcondition FROM Objekttypen, Verleihobjektearchiv, v1_leiharchiv WHERE Objekttypen.id=Verleihobjektearchiv.typID AND Verleihobjektearchiv.id=v1_leiharchiv.objectID ORDER BY returned ASC;");
                $statement->execute(array(":lastDay" => date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d")-1 , date("Y")))));
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                //Erstelle Tabelle mit den Daten
                $html .= "<table class=\"ofb-table\"> <tr class='ofb-tr'>  <th class='ofb-th'>Gegenstand</th> <th class='ofb-th'>Verleiher</th> <th class='ofb-th'>Leihender</th> <th class='ofb-th'>Pfand</th> <th class='ofb-th'>Uhrzeit</th> <th class='ofb-th'>Zustand des Gegenstands</th>  </tr>";
                $i = 0;
                foreach($resArr as $res) {
                    if ($i % 2 == 0) {
                        $oddeven = "even";
                    } else {
                        $oddeven = "odd";
                    }
                    $i++;
                    $obj = $res["Name"] . " ";
                    if ($res["shownr"] == 0) {
                        $obj .= $res["comment"];
                    } else {
                        $obj .= $res["nr"];
                    }
                    $html .= "<tr class='ofb-tr ofb-row-" . $oddeven . "'>  <td class='ofb-td'>" . $obj ."</td> <td class='ofb-td'>" . $res["lender"] . "</td> <td class='ofb-td'>" . $res["lending"] . "</td> <td class='ofb-td'>" . $res["pawn"] . "</td> <td class='ofb-td'>";
                    $unixRent = strtotime($res["time"]);
                    $unixReturn = strtotime($res["returned"]);
                    if (date("Y-m-d", $unixRent) == date("Y-m-d", $unixReturn)) {
                        $html .= date("H:i", $unixRent) . " - " . date("H:i", $unixReturn) . " Uhr, " . date("d.m.Y", $unixReturn);
                    } else {
                        $html .= date("H:i", $unixRent) . " Uhr, " . date("d.m.Y", $unixRent) . " - " . date("H:i", $unixReturn) . " Uhr, " . date("d.m.Y", $unixReturn);
                    }
                    $html .= "</td> <td class='ofb-td'>" . $res["itemcondition"] . "</td>  </tr>";
                }
                $html .= "</table><br>";
                break;

            case "bigarchive":
                //Frage Daten aus der DB ab.
                $statement = $this->dbhandle->connection->prepare("SELECT Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Objekttypen.id AS ObjID, Verleihobjekte.id AS voID, Verleihobjekte.Nummer, Verleihobjekte.Bemerkung, v1_leiharchiv.lender, v1_leiharchiv.lending, v1_leiharchiv.pawn, v1_leiharchiv.time, v1_leiharchiv.returned, v1_leiharchiv.itemcondition FROM Objekttypen, Verleihobjekte, v1_leiharchiv WHERE Objekttypen.id=Verleihobjekte.Typ AND Verleihobjekte.id=v1_leiharchiv.objectID ORDER BY returned DESC;");
                $statement->execute(array());
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                //Erstelle Tabelle mit den Daten
                $html .= "<table class=\"ofb-table\"> <tr class='ofb-tr'>  <th class='ofb-th'>Gegenstand</th> <th class='ofb-th'>Verleiher</th> <th class='ofb-th'>Leihender</th> <th class='ofb-th'>Pfand</th> <th class='ofb-th'>Uhrzeit</th> <th class='ofb-th'>Zustand des Gegenstands</th>  </tr>";
                $i = 0;
                foreach($resArr as $res) {
                    if ($i % 2 == 0) {
                        $oddeven = "even";
                    } else {
                        $oddeven = "odd";
                    }
                    $i++;
                    $obj = $res["Name"] . " ";
                    if ($res["shownr"] == 0) {
                        $obj .= $res["Bemerkung"];
                    } else {
                        $obj .= $res["Nummer"];
                    }
                    $html .= "<tr class='ofb-tr ofb-row-" . $oddeven . "'>  <td class='ofb-td'>" . $obj ."</td> <td class='ofb-td'>" . $res["lender"] . "</td> <td class='ofb-td'>" . $res["lending"] . "</td> <td class='ofb-td'>" . $res["pawn"] . "</td> <td class='ofb-td'>";
                    $unixRent = strtotime($res["time"]);
                    $unixReturn = strtotime($res["returned"]);
                    if (date("Y-m-d", $unixRent) == date("Y-m-d", $unixReturn)) {
                        $html .= date("H:i", $unixRent) . " - " . date("H:i", $unixReturn) . " Uhr, " . date("d.m.Y", $unixReturn);
                    } else {
                        $html .= date("H:i", $unixRent) . " Uhr, " . date("d.m.Y", $unixRent) . " - " . date("H:i", $unixReturn) . " Uhr, " . date("d.m.Y", $unixReturn);
                    }
                    $html .= "</td> <td class='ofb-td'>" . $res["itemcondition"] . "</td>  </tr>";
                }
                $html .= "</table><br>";
                break;
        }
        return $html;
    }


    public function item($opt) {
        $html = "";
        switch($opt) {
            case "list":
                //Frage Daten aus der DB ab.
                $statement = $this->dbhandle->connection->prepare("SELECT Objekttypen.id as tID, Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Nummer, Bemerkung FROM Verleihobjekte, Objekttypen WHERE Verleihobjekte.Typ=Objekttypen.id ORDER BY Name, Nummer, Bemerkung ASC;");
                $statement->execute(array());
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                //Erstelle HTML Tabelle.
                $html .= "<br><table class='ofb-table-thin'> <tr class='ofb-tr'>  <th class='ofb-th'>Gegenstand</th> <th class='ofb-th'>Bemerkung</th>  </tr>";
                $i = 0;
                foreach($resArr as $res) {
                    if ($i % 2 == 0) {
                        $oddeven = "even";
                    } else {
                        $oddeven = "odd";
                    }
                    $i++;
                    $shortname = "";
                    if ($res["shortname"] != "") {
                        $shortname = " (" . $res["shortname"] . ")";
                    }
                    if ($res["shownr"] == 0) {
                        $html .= "<tr class='ofb-tr ofb-row-" . $oddeven . "'>  <td class='ofb-td'>" . $res["Name"] . $shortname . "</td> <td class=*'ofb-td'>" . $res["Bemerkung"] . "</td></tr>";
                    } else {
                        $html .= "<tr class='ofb-tr ofb-row-" . $oddeven . "'>  <td class='ofb-td'>" . $res["Name"] . " " . $res["Nummer"] . $shortname . "</td> <td class=*'ofb-td'>" . $res["Bemerkung"] . "</td></tr>";
                    }
                }
                $html .= "</table><br>";
                break;


            case "add":
                //Überprüfe, ob OFB Admin Berechtigungen vorhanden sind.
                if (!in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"])) {
                    return "<div class=\"warnmsg\">Sie haben nicht die Berechtigung, um diesen Teil zu sehen!</div>";
                }

                //Gegenstandtypen aus DB holen
                $statement = $this->dbhandle->connection->prepare("SELECT id, Name FROM Objekttypen;");
                $statement->execute(array());
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);

                //Dropdown Array bauen
                $dropArr = array();
                foreach($resArr as $res) {
                    array_push($dropArr, "(" . $res["id"] . ") " . $res["Name"]);
                }

                //Erstelle Formular zum Hinzügen eines Gegenstands
                $form = new dokuwiki\Form\Form();
                $form->addDropdown("item_add_type", $dropArr, "Typ:");
                $form->addTextInput("item_add_nr", "Nummer:");
                $form->addTextInput("item_add_comment", "Bemerkung:");
                $form->addButton("submit", "Gegenstand anlegen");
                $html .= $form->toHTML();

                //Wenn ein Gegenstand hinzugefügt werden soll, füge ihn hinzu
                if (((isset($_POST["item_add_nr"]) && $_POST["item_add_nr"] !== "") || (isset($_POST["item_add_comment"]) && $_POST["item_add_comment"] !== "")) && checkSecurityToken()) {
                    //Variablen auslesen, ggf. auf Standard Werte setzen
                    $typID = substr(strstr($_POST["item_add_type"], ")", true), 1);
                    $nr = intval($_POST["item_add_nr"]);
                    $comment = "";
                    $shownr = 1;
                    //Hole typIDs, die keine Nummer gesetzt bekommen
                    $statement = $this->dbhandle->connection->prepare("SELECT id, shownr FROM Objekttypen;");
                    $statement->execute(array());
                    $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                    foreach($resArr as $res) {
                        if($res["id"] == $typID) {
                            $shownr = $res["shownr"];
                        }
                    }
                    if (!$shownr) {
                        $nr=1;
                    }
                    if ($nr==0) {
                        $html .= "<br><div class=\"warnmsg\">Nummer muss eine Zahl > 0 sein!</div>";
                    }
                    if (isset($_POST["item_add_comment"])) {
                        $comment = $_POST["item_add_comment"];
                    }

                    //Statement zum Hinzufügen des Gegenstands
                    $statement = $this->dbhandle->connection->prepare("INSERT INTO `Verleihobjekte` (id, Typ, Nummer, Bemerkung) VALUES (NULL, :typID, :nr, :comment);");
                    $statement->execute(array(":typID" => "$typID", ":nr" => "$nr", ":comment" => "$comment"));
                }
                break;

            
            case "remove":
                //Überprüfe, ob OFB Admin Berechtigungen vorhanden sind.
                if (!in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"])) {
                    return "<div class=\"warnmsg\">Sie haben nicht die Berechtigung, um diesen Teil zu sehen!</div>";
                }

                //Liste aller Gegenstände
                $statement = $this->dbhandle->connection->prepare("SELECT Objekttypen.id as tID, Verleihobjekte.id as oID, Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Nummer, Bemerkung FROM Verleihobjekte, Objekttypen WHERE Verleihobjekte.Typ=Objekttypen.id ORDER BY Name, Nummer, Bemerkung;");
                $statement->execute(array());
                $resArrItems = $statement->fetchAll(PDO::FETCH_ASSOC);
                $labels = array();
                foreach($resArrItems as $item) {
                    if ($this->getConf("ofb-name-short") && $item["shortname"] != "") {
                        $obj = $item["shortname"] . " ";
                    } else {
                        $obj = $item["Name"] . " ";
                    }
                    if ($item["shownr"] == 0) {
                        $obj .= $item["Bemerkung"];
                    } else {
                        $obj .= $item["Nummer"];
                    }
                    array_push($labels, $item["oID"] . "#" . $obj);
                }

                //Erstelle Formular zum auswählen, was gelöscht wird
                $form = new dokuwiki\Form\Form();
                foreach($labels as $labelInfos) {
                    list($id, $label) = explode('#', $labelInfos);
                    $form->addCheckbox("item_remove_object_" . $id, $label);
                    $form->setHiddenField("item_remove", "true");
                    $form->addHTML("&nbsp &nbsp &nbsp"); //FÜge 3 Leerzeichen zwischen die Checkboxen
                }
                $form->addHTML("<br>");
                $form->addButton("submit", "Gegenstände löschen!");
                $html .= $form->toHTML();

                //Wenn Gegenstände gelöscht werden sollen, SQL Statement
                if (isset($_POST["item_remove"]) && $_POST["item_remove"] != "" && checkSecurityToken()) {
                    //IDs Gegenstände holen
                    $statement = $this->dbhandle->connection->prepare("SELECT id FROM Verleihobjekte;");
                    $statement->execute(array());
                    $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                    //Gegenstände auslesen
                    foreach($resArr as $id) {
                        $id = $id["id"];
                        if (isset($_POST["item_remove_object_" . $id]) && $_POST["item_remove_object_" . $id] !== "") {
                            if ($_POST["item_remove_object_" . $id]) {
                                //Frag zu löschende Daten ab
                                $statement = $this->dbhandle->connection->prepare("SELECT * FROM Verleihobjekte WHERE id=?");
                                $statement->execute(array($id));
                                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                                //Abgefragte Daten in das Archiev schreiben
                                foreach($resArr as $res) {
                                    $statement = $this->dbhandle->connection->prepare("INSERT INTO Verleihobjektearchiv (id, typID, nr, comment) VALUES (:id, :typID, :nr, :comment);");
                                    $statement->execute(array(":id" => $res["id"], ":typID" => $res["Typ"], ":nr" => $res["Nummer"], ":comment" => $res["Bemerkung"]));
                                }
                                //Lösche Daten
                                $statement = $this->dbhandle->connection->prepare("DELETE FROM Verleihobjekte WHERE id=?;");
                                $statement->execute(array($id));
                            }
                        }
                    }
                }
                break;


            case "edit":
                //Erstelle ein Array mit den Gegenständen
                $statement = $this->dbhandle->connection->prepare("SELECT Verleihobjekte.id as oID, Objekttypen.id as tID, Objekttypen.Name, Objekttypen.shortname, Objekttypen.shownr, Nummer, Bemerkung FROM Verleihobjekte, Objekttypen WHERE Verleihobjekte.Typ=Objekttypen.id ORDER BY Name, Nummer, Bemerkung ASC;");
                $statement->execute(array());
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                $dropArr = array();
                foreach($resArr as $res) {
                    //if ($this->getConf("ofb-name-short") && $res["shortname"] != "") {
                        //$name = $res["shortname"];
                    //} else {
                        $name = $res["Name"];
                    //}
                    $str = "(" . $res["oID"] . ") " . $name;
                    if ($res["shownr"] == 0) {
                        $str .= " " . $res["Bemerkung"];
                    } else {
                        $str .= " " . $res["Nummer"];
                    }
                    array_push($dropArr, $str);
                }
                //Erstelle ein Formular zum Beschreibung ändern
                $form = new dokuwiki\Form\Form();
                $form->setHiddenField("item_edit", "true");
                $form->addDropdown("item_edit_obj", $dropArr, "Gegenstand auswählen:");
                $form->addHTML("<br>");
                $form->addTextInput("item_edit_comment", "Neue Beschreibung:");
                $form->addHTML("<br>");
                $form->addButton("submit", "Beschreibung ändern");
                $html .= $form->toHTML();

                //Wenn ein Gegenstand geändert werden soll, SQL Statement
                if (isset($_POST["item_edit"]) && $_POST["item_edit"] !== "" && checkSecurityToken()) {
                    //SQL Statement zum bearbeiten der Gegenstände
                    $statement = $this->dbhandle->connection->prepare("UPDATE Verleihobjekte SET Bemerkung = :comment WHERE Verleihobjekte.id = :id;");
                    $statement->execute(array(":comment" => $_POST["item_edit_comment"], ":id" => substr(strstr($_POST["item_edit_obj"], ")", true), 1)));
                }
                break;

            
            case "addtyp":
                //Überprüfe, ob OFB Admin Berechtigungen vorhanden sind.
                if (!in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"])) {
                    return "<div class=\"warnmsg\">Sie haben nicht die Berechtigung, um diesen Teil zu sehen!</div>";
                }

                //Erzeuge Formular zum Gegenstandtyp hinzufügen
                $form = new dokuwiki\Form\Form();
                $form->addTextInput("item_addtyp", "Name:");
                $form->addHTML("<br>");
                $form->addTextInput("item_addtyp_shortname", "Abkürzung:");
                $form->addHTML("<br>");
                $form->addCheckbox("item_addtyp_shownr", "Nummer verstecken, nur Beschreibung anzeigen:");
                $form->addHTML("<br>");
                $form->addButton("submit", "Gegenstandtyp hinzufügen");
                $html .= $form->toHTML();

                //Wenn neuer Typ gesetzte ist SQL Satetment
                if (isset($_POST["item_addtyp"]) && $_POST["item_addtyp"] !== "" && checkSecurityToken()) {
                    //Setzte ggf. default Variablen
                    if (isset($_POST["item_addtyp_shownr"])) {
                        $shownr = 0;
                    } else {
                        $shownr = 1;
                    }
                    //SQL Satetment
                    $statement = $this->dbhandle->connection->prepare("INSERT INTO Objekttypen (id, Name, shortname, shownr) VALUES (NULL, :longname, :shortname, :shownr);");
                    $statement->execute(array(":longname" => $_POST["item_addtyp"], ":shortname" => $_POST["item_addtyp_shortname"], ":shownr" => $shownr));
                }
                break;
        }
        return $html;
    }


    public function credit($opt) {
        $html = "";
        //ist der User Admin?
        $admin = in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"]);
        //Frage Daten aus DB ab.
        $statement = $this->dbhandle->connection->prepare("SELECT id, firstname as Vorname, lastname as Nachname, amount as Guthaben, volunteer AS ehrenamtlich FROM v1_credit ORDER BY firstname ASC;");
        $statement->execute(array());
        $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
        switch($opt) {
            case "addperson";
                //Überprüfe, ob OFB Admin Berechtigungen vorhanden sind.
                if (!in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"])) {
                    return "<div class=\"warnmsg\">Sie haben nicht die Berechtigung, um diesen Teil zu sehen!</div>";
                }

                //Erstelle Formular zum Hinzufügen einer Person.
                $form = new dokuwiki\Form\Form();
                $form->addTextInput("credit_addperson_firstname", "Vorname:");
                $form->addHTML("<br>");
                $form->addTextInput("credit_addperson_lastname", "Nachname:");
                $form->addHTML("<br>");
                $form->addCheckbox("credit_addperson_volunteer", "ehrenamtlich:");
                $form->addHTML("<br>");
                $form->addButton("submit", "Person anlegen");
                $html .= $form->toHTML();

                //Wenn die Parameter zum anlegen einer Persn gesetzt sind, dann Person anlegen
                if (isset($_POST["credit_addperson_firstname"]) && $_POST["credit_addperson_firstname"] !== "" && isset($_POST["credit_addperson_lastname"]) && $_POST["credit_addperson_lastname"] !== "" && checkSecurityToken()) {
                    //SQL Statement zum Person anlegen
                    $statement = $this->dbhandle->connection->prepare("INSERT INTO v1_credit (id, firstname, lastname, amount, volunteer) VALUES (NULL, :firstname, :lastname, 0, :volunteer);");
                    $statement->execute(array(":firstname" => $_POST["credit_addperson_firstname"], ":lastname" => $_POST["credit_addperson_lastname"], ":volunteer" => intval(isset($_POST["credit_addperson_volunteer"]))));
                }
                break;


            case "edit":
                //Überprüfe, ob OFB Admin Berechtigungen vorhanden sind.
                if (!in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"])) {
                    return "<div class=\"warnmsg\">Sie haben nicht die Berechtigung, um diesen Teil zu sehen!</div>";
                }

                //Erstelle ein Formular, zum Bearbeiten des Status und des Namens der Person
                $form = new dokuwiki\Form\Form();
                $dropdown = $form->addDropdown("credit_person_edit", array(), "Person:");

                //Erzeuge Array für das Dropdown Menü der Namen.
                $dropArr = array();
                $resArr = $this->filterNames($resArr, $admin);
                foreach($resArr as $res) {
                    if ($admin) {
                        $str = "(" . $res["id"] . ") " . $res["Vorname"] . " " . $res["Nachname"] . ": " . $res["Guthaben"] . "€";
                    } else {
                        $str = "(" . $res["id"] . ") " . $res["Vorname"] . ": " . $res["Guthaben"] . "€";
                    }
                    array_push($dropArr, $str);
                }
                $dropdown->options($dropArr);
                $form->addHTML("<br>");
                $form->addTextInput("credit_person_firstname", "Vorname:");
                $form->addHTML("<br>");
                $form->addTextInput("credit_person_lastname", "Nachname:");
                $form->addHTML("<br>");
                $form->addCheckbox("credit_person_volunteer", "ehrenamtlich:");
                $form->addHTML("<br>");
                $form->addButton("submit", "Person bearbeiten!");
                $html .= $form->toHTML();


                //Wenn Werte zum Bearbeiten gesetzt sind, dann passe die DB an.
                if (isset($_POST["credit_person_firstname"]) && $_POST["credit_person_firstname"] !== "") {
                    //Speichere die Werte
                    $id = substr(strstr($_POST["credit_person_edit"], ")", true), 1);
                    if ($id != "") {
                        $statement = $this->dbhandle->connection->prepare("UPDATE v1_credit SET firstname = :firstname WHERE v1_credit.id = :id ");
                        $statement->execute(array(":id" => $id, ":firstname" => $_POST["credit_person_firstname"]));
                    }
                }
                if (isset($_POST["credit_person_lastname"]) && $_POST["credit_person_lastname"] !== "") {
                    //Speichere die Werte
                    $id = substr(strstr($_POST["credit_person_edit"], ")", true), 1);
                    if ($id != "") {
                        $statement = $this->dbhandle->connection->prepare("UPDATE v1_credit SET lastname = :lastname WHERE v1_credit.id = :id ");
                        $statement->execute(array(":id" => $id, ":lastname" => $_POST["credit_person_lastname"]));
                    }
                }
                if (isset($_POST["credit_person_volunteer"])) {
                    //Speichere die Werte
                    $id = substr(strstr($_POST["credit_person_edit"], ")", true), 1);
                    if ($id != "") {
                        $statement = $this->dbhandle->connection->prepare("UPDATE v1_credit SET volunteer = :volunteer WHERE v1_credit.id = :id ");
                        $statement->execute(array(":id" => $id, ":volunteer" => isset($_POST["credit_person_volunteer"])));
                    }
                }
                break;


            case "list":
                //Filtere Personen mit Doppeltem Nachnamen raus und füge so lange Buchstaben des Nachnamens an der Vornamen, bis die unterschiedlich sind.
                $newResArr = $this->filterNames($resArr, $admin);
                //Erstelle HTML Tabelle.
                $lastname = "";
                $firstname = "Name";
                if ($admin) {
                    $lastname = "<th class='ofb-th'>Nachname</th>";
                    $firstname = "Vorname";
                }
                $html .= "<br><table class='ofb-table' style='width: 80%;'> <tr class='ofb-tr'>  <th class='ofb-th'>$firstname</th> $lastname <th class='ofb-th'>Ehrenamtlicher</th>  <th class='ofb-th'>Guthaben</th>  </tr>";
                $i = 0;
                foreach($newResArr as $res) {
                    if ($i % 2 == 0) {
                        $oddeven = "even";
                    } else {
                        $oddeven = "odd";
                    }
                    $i++;
                    $color = "-red";
                    if ($res["Guthaben"] >= 0) {
                        $guthabenStr = "<strong>+&nbsp</strong>" . abs($res["Guthaben"]);
                        $color = "";
                    } else {
                        $guthabenStr = "<strong>-&nbsp</strong>" . abs($res["Guthaben"]);
                        if (!(abs($res["Guthaben"]) > $this->getConf("creditlimit")) && $res["ehrenamtlich"]) {
                            $color = "";
                        }
                        if (abs($res["Guthaben"]) == $this->getConf("creditlimit") && $res["ehrenamtlich"]) {
                            $color = "-orange";
                        }
                    }
                    $lastname = "";
                    if ($admin) {
                        $lastname = "<td class='ofb-td'>" . $res["Nachname"] . "</td>";
                    }
                    $volunteer = "Nein";
                    if ($res["ehrenamtlich"]) {
                        $volunteer = "Ja";
                    }
                    $html .= "<tr class='ofb-tr ofb-row-" . $oddeven . $color . "'>  <td class='ofb-td'>" . $res["Vorname"] . "</td> $lastname <td class='ofb-td'> $volunteer </td>  <td class='ofb-td'>" . $guthabenStr . "€</td></tr>";
                }
                $html .= "</table><br>";
                break;


            case "add":
                //Erstelle Formular.
                $form = new dokuwiki\Form\Form();
                $form->addTextInput("credit_add_contributor", "Mitarbeiter:");
                $dropdown = $form->addDropdown("credit_person_add", array(), "Person:");

                //Erzeuge Array für das Dropdown Menü der Namen.
                $dropArr = array();
                /*foreach($resArr as $res) {
                    $idString = $res["id"];
                    $labelString = $res["Vorname"] . " " . $res["Nachname"] . ": " . $res["Guthaben"] . "€";
                    $push_array = array($idString => $labelString);
                    $dropArr = array_merge($dropArr, $push_array);
                }*/
                $resArr = $this->filterNames($resArr, $admin);
                foreach($resArr as $res) {
                    if ($admin) {
                        $str = "(" . $res["id"] . ") " . $res["Vorname"] . " " . $res["Nachname"] . ": " . $res["Guthaben"] . "€";
                    } else {
                        $str = "(" . $res["id"] . ") " . $res["Vorname"] . ": " . $res["Guthaben"] . "€";
                    }
                    array_push($dropArr, $str);
                }
                $dropdown->options($dropArr);

                $form->addTextInput("credit_amount_add", "Betrag:");
                $form->addButton("submit", "Einzahlen");
                $html .= $form->toHTML();

                //Wenn die Einzahloptionen gesetzt sind, dann Geld einzahlen
                if (isset($_POST["credit_amount_add"]) && $_POST["credit_amount_add"] !== "" && $_POST["credit_amount_add"] !== "0" && isset($_POST["credit_add_contributor"]) && $_POST["credit_add_contributor"] !== "" && checkSecurityToken()) {
                    //Speichere die Werte für die Einzahlung
                    $id = substr(strstr($_POST["credit_person_add"], ")", true), 1);
                    $addAmount = floatval($_POST["credit_amount_add"]);
                    $contributor = $_POST["credit_add_contributor"];
                    if ($id != "") {
                        //Eintrag ins Archiv machen
                        $statement = $this->dbhandle->connection->prepare("INSERT INTO v1_creditarchive (id, pID, amount, contributor, edited) VALUES (NULL, :pID, :amount, :contributor, :edited);");
                        $statement->execute(array(":pID" => $id, ":amount" => $addAmount, ":contributor" => $contributor, ":edited" => date("Y-m-d H:i:s")));
                        //aktuellen Betrag holen
                        $statement = $this->dbhandle->connection->prepare("SELECT amount FROM v1_credit WHERE id=:id;");
                        $statement->execute(array(":id" => "$id"));
                        $curAmountArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                        $curAmount = $curAmountArr[0]["amount"];
                        //Berechne neuen Betrag
                        $newAmount = $curAmount + $addAmount;
                        //SQL Satement zum einzahlen
                        $statement = $this->dbhandle->connection->prepare("UPDATE v1_credit SET amount = :amount WHERE v1_credit.id = :id;");
                        $statement->execute(array(":amount" => "$newAmount", ":id" => "$id"));
                    }
                }
                break;


            case "sub":
                //Erstelle Formular.
                $form = new dokuwiki\Form\Form();
                $form->addTextInput("credit_sub_contributor", "Mitarbeiter:");
                $dropdown = $form->addDropdown("credit_person_sub", array(), "Person:");

                //Erzeuge Array für das Dropdown Menü der Namen.
                $dropArr = array();
                /*foreach($resArr as $res) {
                    $idString = $res["id"];
                    $labelString = $res["Vorname"] . " " . $res["Nachname"] . ": " . $res["Guthaben"] . "€";
                    $push_array = array($idString => $labelString);
                    $dropArr = array_merge($dropArr, $push_array);
                }*/
                $resArr = $this->filterNames($resArr, $admin);
                foreach($resArr as $res) {
                    if ($admin) {
                        $str = "(" . $res["id"] . ") " . $res["Vorname"] . " " . $res["Nachname"] . ": " . $res["Guthaben"] . "€";
                    } else {
                        $str = "(" . $res["id"] . ") " . $res["Vorname"] . ": " . $res["Guthaben"] . "€";
                    }
                    array_push($dropArr, $str);
                }
                $dropdown->options($dropArr);

                $form->addTextInput("credit_amount_sub", "Betrag:");
                $form->addButton("submit", "Auszahlen");
                $html .= $form->toHTML();

                //Wenn die Einzahloptionen gesetzt sind, dann Geld einzahlen
                if (isset($_POST["credit_amount_sub"]) && $_POST["credit_amount_sub"] !== "" && $_POST["credit_amount_sub"] !== "0" && isset($_POST["credit_sub_contributor"]) && $_POST["credit_sub_contributor"] !== "" && checkSecurityToken()) {
                    //speichere die Werte für die Einzahlung
                    $id = substr(strstr($_POST["credit_person_sub"], ")", true), 1);
                    $addAmount = floatval($_POST["credit_amount_sub"]);
                    $contributor = $_POST["credit_sub_contributor"];
                    if ($id != "") {
                        //Überprüfe, ob die Person ehrenamtlich aktiv ist und hole den aktuellen Betrag
                        $statement = $this->dbhandle->connection->prepare("SELECT amount, volunteer FROM v1_credit WHERE id=:id;");
                        $statement->execute(array(":id" => "$id"));
                        $curAmountArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                        $curAmount = $curAmountArr[0]["amount"];
                        $volunteer = $curAmountArr[0]["volunteer"];
                        //Berechne neuen Betrag
                        $newAmount = $curAmount - $addAmount;
                        //Überprüfe, ob die Schulden gemacht werden dürfen
                        if ($newAmount < -1 * $this->getConf("creditlimit") && (!in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"]))) { //Keine Admin Rechte und der Betrag ist zu groß
                            //Warnung ausgeben
                            msg("Du hast zu viele Schulden! Du musst diese erst zurückzahlen!");
                        //Überprüfe, ob die Person Schulden macht, wenn ja muss die ehrenamtlich sein.
                        } else if ($newAmount < 0 && !$volunteer) {
                            msg("Nur Ehrenamtliche können Schulden machen!");
                        } else {
                            //Eintrag ins Archiv machen
                            $statement = $this->dbhandle->connection->prepare("INSERT INTO v1_creditarchive (id, pID, amount, contributor, edited) VALUES (NULL, :pID, :amount, :contributor, :edited)");
                            $statement->execute(array(":pID" => $id, ":amount" => $addAmount*-1, ":contributor" => $contributor, ":edited" => date("Y-m-d H:i:s")));
                            //SQL Satement zum einzahlen
                            $statement = $this->dbhandle->connection->prepare("UPDATE v1_credit SET amount = :amount WHERE v1_credit.id = :id;");
                            $statement->execute(array(":amount" => "$newAmount", ":id" => "$id"));
                        }
                    }
                }
                break;


            $big = false;
            case "bigarchive":
                $big = true;


            case "archive":
                //Überprüfe, ob OFB Admin Berechtigungen vorhanden sind.
                if (!in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"])) {
                    return "<div class=\"warnmsg\">Sie haben nicht die Berechtigung, um diesen Teil zu sehen!</div>";
                }
                
                //Frage Daten aus der DB ab.
                if (!$big) {
                    $statement = $this->dbhandle->connection->prepare("SELECT firstname as Vorname, lastname as Nachname, v1_credit.amount as Guthaben, contributor as Mitarbeiter, v1_creditarchive.amount as changedAmount, edited FROM v1_credit, v1_creditarchive WHERE v1_credit.id=v1_creditarchive.pID AND edited>:lastWeek ORDER BY edited DESC;");
                    $statement->execute(array(":lastWeek" => date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d")-7 , date("Y")))));
                } else {
                    $statement = $this->dbhandle->connection->prepare("SELECT firstname as Vorname, lastname as Nachname, v1_credit.amount as Guthaben, contributor as Mitarbeiter, v1_creditarchive.amount as changedAmount, edited FROM v1_credit, v1_creditarchive WHERE v1_credit.id=v1_creditarchive.pID ORDER BY edited DESC;");
                    $statement->execute(array());
                }
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                //Gehe die Daten durch und schreibe sie in eine Tabelle
                $html .= "<table class=\"ofb-table-thin\"> <tr class='ofb-tr'>  <th class='ofb-th'>Mitarbeiter</th> <th class='ofb-th'>Person</th> <th class='ofb-th'>Betrag der Änderung</th> <th class='ofb-th'>Zeitstempel</th>  </th>";
                $i = 0;
                foreach($resArr as $res) {
                    if ($i % 2 == 0) {
                        $oddeven = "even";
                    } else {
                        $oddeven = "odd";
                    }
                    $i++;
                    $html .= "<tr class='ofb-tr ofb-row-" . $oddeven . "'>";
                    $html .= "<td class='ofb-td'>" . $res["Mitarbeiter"] . "</td>";
                    $html .= "<td class='ofb-td'>" . $res["Vorname"] . " " . $res["Nachname"] . "</td>";
                    if ($res["changedAmount"] < 0) {
                        $changedAmount = "<strong>-&nbsp</strong>" . abs($res["changedAmount"]);
                    } else {
                        $changedAmount = "<strong>+&nbsp</strong>" . abs($res["changedAmount"]);
                    }
                    $html .= "<td class='ofb-td'>" . $changedAmount . "</td>";
                    $unixEdited = strtotime($res["edited"]);
                    $html .= "<td class='ofb-td'>" . date("H:i", $unixEdited) . " Uhr, " . date("d.m.Y", $unixEdited) . "</td>";
                    $html .= "</tr>";
                }
                $html .= "</table>";
                break;


            case "liabilities":
                //Überprüfe, ob OFB Admin Berechtigungen vorhanden sind.
                if (!in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"])) {
                    return "";
                }

                //Frage Daten aus der DB ab.
                $statement = $this->dbhandle->connection->prepare("SELECT SUM(amount) AS Schulden FROM v1_credit WHERE amount < 0;");
                $statement->execute(array());
                $amount = $statement->fetchAll(PDO::FETCH_ASSOC)[0]["Schulden"];
                $html = "<div class=\"wrap_center wrap_round wrap_alert plugin_wrap\" style=\"width: 80%;\">Das Jugendhaus hat momentan <strong>" . number_format(abs($amount), 2, ',', '.') . "€</strong> zu wenig in der Kasse. Das ist die Summe der Schulden.</div>";
                break;
        }
        return $html;
    }


    public function rooms($opt) {
        $html = "";
        $big = false;
        switch($opt) {


            case "biglist":
                $big = true;

            case "list":
                //Frage Daten aus der DB ab:
                if ($big) {
                    $statement = $this->dbhandle->connection->prepare("SELECT name, person, start, end FROM v1_bookings, v1_room WHERE v1_bookings.RID=v1_room.RID ORDER BY start DESC;");
                    $statement->execute(array());
                } else {
                    $statement = $this->dbhandle->connection->prepare("SELECT name, person, start, end FROM v1_bookings, v1_room WHERE v1_bookings.RID=v1_room.RID AND start > :today ORDER BY start;");
                    $statement->execute(array(":today" => date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") , date("Y")))));
                }
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);

                //Erstelle eine Tabelle mit den Reservierungen
                $html .= "<table class=\"ofb-table\"> <tr class='ofb-tr'>  <th class='ofb-th'>Raum</th>  <th class='ofb-th'>Person(en)</th>  <th class='ofb-th'>Von</th>  <th class='ofb-th'>Bis</th> </tr>";
                $i = 0;
                foreach($resArr as $res) {
                    if ($i % 2 == 0) {
                        $oddeven = "even";
                    } else {
                        $oddeven = "odd";
                    }
                    $i++;
                    $from = date("d.m.Y H:i", strtotime($res["start"]));
                    $to = date("d.m.Y H:i", strtotime($res["end"]));
                    if ($res["end"] == null) {
                        $to = "-";
                    }
                    $html .= "<tr class='ofb-tr ofb-row-" . $oddeven . "'> <td class='ofb-td'>" . $res["name"] . "</td>  <td class='ofb-td'>" . $res["person"] . "</td>  <td class='ofb-td'>$from</td>  <td class='ofb-td'>$to</td> </tr>";
                }
                $html .= "</table>";
                break;


            case "book":
                //Frage die Räume aus der DB ab:
                $statement = $this->dbhandle->connection->prepare("SELECT RID, name FROM v1_room;");
                $statement->execute(array());
                $resArr = $statement->fetchAll(PDO::FETCH_ASSOC);
                //Erstelle ein Formular zum Reservieren
                $form = new dokuwiki\Form\Form();
                $dropdown = $form->addDropdown("rooms_book_rid", array(), "Raum:");
                $options = array();
                foreach($resArr as $res) {
                    array_push($options, "(" . $res["RID"] . ") " . $res["name"]);
                }
                $dropdown->options($options);
                $form->addTextInput("rooms_book_person", "Person(en):");
                //Handle die Zeiträume ab: Zuerst den Startzeitpunkt
                $form->addHTML("<br><div class=\"ofb-text\"><strong>Zeitraum:</strong> <br> <table><tr><td class=\"ofb-space-td\"><strong>Start:</strong></td> <td class=\"ofb-space-td\">");
                $form->addCheckbox("rooms_book_start_now", "Jetzt");
                $form->addHTML("</td><td class=\"ofb-space-td\"></td></tr>  <tr><td class=\"ofb-space-td\"></td> <td class=\"ofb-space-td\"><strong>Manuell eingeben:</strong></td> <td class=\"ofb-space-td\">");
                $days = array();
                for($i=1; $i<32; $i++) {
                    if($i<10) {$i = "0" . $i;}
                    array_push($days, $i);
                }
                $months = array();
                for($i=intval(date("m")); $i<13; $i++) {
                    if ($i<10) {$i = "0" . $i;}
                    array_push($months, $i);
                }
                $years = array(date("Y"));
                $form->addDropdown("rooms_book_start_day", $days, "Tag:");
                $form->addDropdown("rooms_book_start_month", $months, "Monat:");
                $form->addDropdown("rooms_book_start_year", $years, "Jahr:");
                $form->addHTML("</td></tr>  <tr class=\"ofb-space-tr\"><td class=\"ofb-space-td\"></td><td class=\"ofb-space-td\"></td> <td class=\"ofb-space-td\">");
                $hours = array();
                for($i=0; $i<24; $i++) {
                    if ($i<10) {$i =  "0" . $i;}
                    array_push($hours, $i);
                }
                $minutes = array();
                for($i=0; $i<60; $i++) {
                    if ($i<10) {$i =  "0" . $i;}
                    array_push($minutes, $i);
                }
                $form->addDropdown("rooms_book_start_hour", $hours, "Uhrzeit:");
                $form->addDropdown("rooms_book_start_minute", $minutes, ":");
                $form->addHTML("<strong>Uhr</strong><td></tr></table></div>");
                //Handle den Endzeitpunkt ab
                $form->addHTML("<div class=\"ofb-text\"> <table><tr><td class=\"ofb-space-td\"><strong>Ende:</strong></td> <td class=\"ofb-space-td\">");
                $form->addCheckbox("rooms_book_end_open", "Das Ende ist noch nicht festgelegt.");
                $form->addHTML("</td><td class=\"ofb-space-td\"></td></tr>  <tr><td class=\"ofb-space-td\"></td> <td class=\"ofb-space-td\"><strong>Manuell eingeben:</strong></td> <td class=\"ofb-space-td\">");
                $form->addDropdown("rooms_book_end_day", $days, "Tag:");
                $form->addDropdown("rooms_book_end_month", $months, "Monat:");
                $form->addDropdown("rooms_book_end_year", $years, "Jahr:");
                $form->addHTML("</td></tr>  <tr class=\"ofb-space-tr\"><td class=\"ofb-space-td\"></td><td class=\"ofb-space-td\"></td> <td class=\"ofb-space-td\">");
                $form->addDropdown("rooms_book_end_hour", $hours, "Uhrzeit:");
                $form->addDropdown("rooms_book_end_minute", $minutes, ":");
                $form->addHTML("<strong>Uhr</strong><td></tr></table></div>");
                $form->addButton("submit", "reservieren!");
                $html .= $form->toHTML();


                //Wenn Daten zum Reservieren gesetzte sind, bastle einen Eintrag für die BD.
                if (isset($_POST["rooms_book_person"]) && $_POST["rooms_book_person"] !== "") {
                    if (isset($_POST["rooms_book_start_now"])) {
                        $start = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d") , date("Y")));
                    } else {
                        $start = date("Y-m-d H:i:s", mktime($_POST["rooms_book_start_hour"], $_POST["rooms_book_start_minute"], 0, $_POST["rooms_book_start_month"], $_POST["rooms_book_start_day"] , $_POST["rooms_book_start_year"]));
                    }
                    if (isset($_POST["rooms_book_end_open"])) {
                        $end = null;
                    } else {
                        $end = date("Y-m-d H:i:s", mktime($_POST["rooms_book_end_hour"], $_POST["rooms_book_end_minute"], 0, $_POST["rooms_book_end_month"], $_POST["rooms_book_end_day"] , $_POST["rooms_book_end_year"]));
                    }
                    //Wenn der gewählte Zeitraum gültig ist
                    if ($start >= date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d") , date("Y"))) && ($end == null || $start < $end)) {
                        //Bereite das Statement für die DB vor
                        $statement = $this->dbhandle->connection->prepare("INSERT INTO v1_bookings (BID, RID, person, start, end) VALUES (NULL, :RID, :person, :start, :end)");
                        $statement->execute(array(":RID" => substr(strstr($_POST["rooms_book_rid"], ")", true), 1), ":person" => $_POST["rooms_book_person"], ":start" => $start, ":end" => $end));
                    }
                }
                break;

            
            case "reverse":
                if (!in_array("ofbadmin", $GLOBALS["INFO"]["userinfo"]["grps"])) {
                    $html .= "<div class=\"wrap_center wrap_round wrap_important plugin_wrap\" style=\"width: 80%;\">Du kannst Reservierungen nur von einem Hauptamtlichem Mitarbeiter/in löschen lassen.</div>";
                }
                break;

        }
        return $html;
    }


    public function filterNames($resArr, $admin) {
        //Filtere Personen mit Doppeltem Nachnamen raus und füge so lange Buchstaben des Nachnamens an der Vornamen, bis die unterschiedlich sind.
        $newResArr = $resArr;
        if (!$admin) {
            //Filtere die Namen raus, die bei gleichem Vornamen unterschiedliche Nachnamen haben.
            $equalFirstnames = array();
            for($i = 0; $i < sizeof($newResArr); $i++) {
                $res = $newResArr[$i];
                foreach($resArr as $oldRes) {
                    if ($res["Vorname"] == $oldRes["Vorname"] && $res["Nachname"] != $oldRes["Nachname"]) {
                        array_push($equalFirstnames, array("Index" => $i, "Vorname" => $res["Vorname"], "Nachname" => $res["Nachname"]));
                        $res["Vorname"] = $res["Vorname"] . " " . $res["Nachname"];
                    }
                }
                $newResArr[$i] = $res;
            }
            //print("EqualFirstnames: <br>");
            //var_dump($equalFirstnames);
            //print("<br>");

            //Füge bei den gefilterten Namen zum Vornamen Teile des Nachnames dazu.
            for($j = 0; $j < sizeof($equalFirstnames); $j++) {
                //print("Index: $j<br>");
                $arr1 = $equalFirstnames[$j];
                $l = strlen($arr1["Nachname"]);
                $curLongest = "";
                foreach($equalFirstnames as $arr2) {
                    //print($arr2["Nachname"][1] . "; " . $arr2["Nachname"] . "<br>");
                    if ($arr1["Nachname"] != $arr2["Nachname"]) {
                        $dead = false;
                        for($i = 0; $i<$l && !$dead; $i++) {
                            if ($i < strlen($arr2["Nachname"]) && $arr1["Nachname"][$i] != $arr2["Nachname"][$i]) {
                                //print("Nachname1: " . $arr1["Nachname"] . "; Nachname2: " . $arr2["Nachname"] . "; BuchstabeNr: $i;Buchstabe1: " . $arr1["Nachname"][$i] . "; Buchstabe2: " . $arr2["Nachname"][$i]) . "<br>";
                                $dead = true;
                                $n1k = substr($arr1["Nachname"], 0, $i+1);
                                $l1k = strlen($n1k);
                                $n2k = substr($arr2["Nachname"], 0, $i+1);
                                $l2k = strlen($n2k);
                                //print("Nachname1Kurz: $n1k; Länge: $l1k; Nachname2Kurz: $n2k; Länge: $l2k;<br>");
                                if (strlen($curLongest) < $l1k) {
                                    $curLongest = $n1k;
                                    //print("New Longest<br>");
                                }
                            }
                        }
                        //print("<br>");
                    }
                }
                $arr1["Vorname"] = $arr1["Vorname"] . " $curLongest.";
                $equalFirstnames[$j] = $arr1;
                //print("Abkürzung von " . $arr1["Nachname"] . ": $curLongest<br>");
                //print("<br><br>");
            }
            //print("Nach Kürzung:<br>");
            //var_dump($equalFirstnames);

            //Füge die Bearbeiteten Namen wieder ins gesamtArray ein
            foreach($equalFirstnames as $arr) {
                $newResArr[$arr["Index"]]["Vorname"] = $arr["Vorname"];
            }
        }
        //print("<br>-----------<br>");
        return $newResArr;
    }

}

