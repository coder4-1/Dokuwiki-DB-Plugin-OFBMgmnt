<?php
/**
 * Options for the projekt plugin
 *
 * @author Marcel Griesbach <ml@grsbch.de>
 */


//$meta['fixme'] = array('string');

$meta['dbhost'] = array('string');
$meta['dbname'] = array('string');
$meta['dbusername'] = array('string');
$meta['dbuserpassword'] = array('string');
$meta['ofb-name-short'] = array('onoff');
$meta['creditlimit'] = array('numeric');