<?php
/**
 * Default settings for the projekt plugin
 *
 * @author Marcel Griesbach <ml@grsbch.de>
 */

//$conf['fixme']    = 'FIXME';

$conf['dbhost']    = '';
$conf['dbname']    = '';
$conf['dbusername']    = '';
$conf['dbuserpassword']    = '';
$conf['ofb-name-short'] = 1;
$conf['creditlimit'] = 5;